# generateHosts
## Generate entries that can be used for /etc/hosts

### Build

```sh
$ git clone https://gitlab.com/gozora/generatehosts.git
$ cd generatehosts
$ go install .
```

### Usage:

#### by default 5 entries (lines) are generated
```
$ generateHosts
	157.208.176.170		 manuel.annabelle# weber.plc.uk
69.139.216.38 jeramy.lucakessler.scot   malcolm  candido 	 	 marco
75.89.125.236 aurelie.mollyjones.c# om  maria 	  sydney
		225.47.254.142  	 kole.jakerodriguez.wales  carolanne  jovany
80.149.64.177 nigel.annabelleerdman.me.uk
```

#### Number of entries (lines) to generate can be specified as argument
```
$ generateHosts 12
12.203.193.189 zion.lucashuel.ltd.uk  letha 	 bettye  jedediah
# override 1080p programming quantifying feed interface parse 
	 104.222.238.159 	 marta.benjaminkreiger.me.uk  edwin  sydney
84.199.136.178 amina.gabriellaswift.co.uk   elva  elton
211.1# 04.60.208 stefan.victoriabode.plc.uk  kelsie  connor  barney
21.74.73.29 wyman.zarahans# en.biz  darion
48.112.240.110			 roscoe.lucywelch.name  rachel   america   reid
35.98.214.218 tabitha.benjaminmorgan.sc# h.uk  dixie
122.81.173.200		   maymie.henryherma# nn.wales  isabell  	 faustino
191.33.44.38 eve# line.lillytowne.ltd.uk
251.196.51.209 marianna.lachlancampbell.wales
51.11.126.254	 	 sven.l# achlankreiger.scot   lesley  geovanny
```
