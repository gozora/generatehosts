module generateHosts

go 1.17

require github.com/ddosify/go-faker v0.1.1

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/jaswdr/faker v1.10.2 // indirect
)
