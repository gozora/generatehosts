package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/ddosify/go-faker/faker"
)

var fkr = faker.NewFaker()

func flipCoin(percent int) bool {
	if rand.Intn(100) < percent-1 {
		return true
	} else {
		return false
	}
}

func randomSpace() string {
	var prefix []rune

	// Throw coin (return 0 | 1)
	// 50% chance that no space/tab will be generated.
	if flipCoin(50) {
		return ""
	}

	// Generate 0-4 spaces or tabs.
	for i := 0; i < rand.Intn(5); i++ {
		if flipCoin(50) {
			prefix = append(prefix, ' ')
		} else {
			prefix = append(prefix, '\t')
		}
	}

	return string(prefix)
}

type hostsLine string

func (hl *hostsLine) randomHostNames(cnt int) {
	// Create FQDN entry
	*hl += hostsLine(fmt.Sprintf("%s %s.%s", randomSpace(),
		strings.ToLower(fkr.RandomPersonFirstName()), fkr.RandomDomainName()))

	// Append short names a.k.a aliases
	for i := 0; i < cnt; i++ {
		*hl += hostsLine(fmt.Sprintf(" %s %s",
			randomSpace(), strings.ToLower(fkr.RandomPersonFirstName())))
	}
}

func (hl *hostsLine) generate(cnt int) {
	// Reset variable content, to avoid hording entries when running in loop.
	*hl = ""

	*hl += hostsLine(randomSpace())
	*hl += hostsLine(fkr.RandomIP())
	hl.randomHostNames(cnt)
}

func main() {
	rand.Seed(time.Now().Unix())
	var cnt int = 5
	var err error

	// Allow passing number of lines as command line argument.
	if len(os.Args) >= 2 {
		cnt, err = strconv.Atoi(os.Args[1])
		if err != nil {
			log.Fatal(err)
		}
	}

	var line hostsLine
	for i := 0; i < cnt; i++ {
		// Generate one hosts line with FQDN and 3 shortnames (aliases)
		line.generate(rand.Intn(4))

		// Comment out part of line with 40% probability.
		if flipCoin(40) {
			pos := rand.Intn(len(line))
			line = line[:pos] + "# " + line[pos:]
		}

		// 10% probability to insert full line comment.
		if flipCoin(10) {
			line = hostsLine(fmt.Sprintf("# %s%s", fkr.RandomWords(),
				fkr.RandomWords()))
		}

		fmt.Print(line)
		fmt.Println()
	}
}
